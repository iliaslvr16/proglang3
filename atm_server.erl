% An ATM model
-module(atm_server).
-behaviour(gen_server).
-import(gen_server, [start/4, stop/1]).
-import(atm, [widthdraw/2]).

-export([init/1, terminate/2, 
         handle_call/3, handle_cast/2,
         handle_info/2, code_change/3,
         start_server/0]).

-define(BANKNOTES_SET, [5000, 50, 50, 50, 1000, 5000, 1000, 500, 100]).

terminate(_Reason, _State) -> ok.
handle_cast(_Request, _State) -> ok.
handle_info(_Info, _State) -> ok.
code_change(_OldVersion, State, _Extra) -> {ok, State}.

start_server() ->
    start({global, atm_server}, ?MODULE, [], []).

init(Args) -> {ok, Args}.

handle_call({widthdraw, Amount}, _From, []) ->
    {Answ, Result, RestBanknotes} = widthdraw(Amount, ?BANKNOTES_SET),
    {reply, {Answ, Result}, RestBanknotes};
handle_call({widthdraw, Amount}, _From, [-1]) ->
    stop(atm_server);
handle_call({widthdraw, Amount}, _From, State) ->
    {Answ, Result, RestBanknotes} = widthdraw(Amount, State),
    if
        RestBanknotes == [] -> {reply, {Answ, Result}, [-1]};
        true -> {reply, {Answ, Result}, RestBanknotes}
    end.
