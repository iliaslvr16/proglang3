% An ATM model
-module(atm).
-import(lists, [min/1, sum/1, sort/1, reverse/1]).
-export([widthdraw/2]).

-define(BANKNOTES_VALS, [50, 100, 500, 1000, 5000]).

widthdraw(Amount, Banknotes) ->
    Min_Banknote = min(?BANKNOTES_VALS),
    Sum_Banknotes = sum(Banknotes),
    if 
        length(Banknotes) < 1 -> error("No banknotes");
        Amount < 0 -> return_fail(Banknotes);
        Amount > Sum_Banknotes -> return_fail(Banknotes);
        Amount rem Min_Banknote /= 0 -> return_fail(Banknotes);
        true -> add_banknote(Amount, [], [], reverse(sort(Banknotes)))
    end.
    
return_fail(Banknotes) ->
    {request_another_amount, [], Banknotes}.

add_banknote(0, Res_banknotes, Rest_banknotes, Banknotes) ->
    {ok, Res_banknotes, Rest_banknotes ++ Banknotes};
add_banknote(_, Res_banknotes, Rest_banknotes, []) ->
    {request_another_amount, [], Res_banknotes ++ Rest_banknotes};
add_banknote(Amount, Res_banknotes, Rest_banknotes, Banknotes) ->
    [Head | Tail] = Banknotes,
    if
        Amount div Head > 0 ->
            add_banknote(Amount - Head, 
                         Res_banknotes ++ [Head],
                         Rest_banknotes, 
                         Tail);
        true -> 
            add_banknote(Amount, 
                         Res_banknotes,
                         Rest_banknotes ++ [Head], 
                         Tail)
    end.
